/*
O objetivo é fazer um juiz de Jokenpo que dada a jogada dos dois jogadores informa o resultado da partida.

As regras são as seguintes:

Pedra empata com Pedra e ganha de Tesoura
Tesoura empata com Tesoura e ganha de Papel
Papel empata com Papel e ganha de Pedra
*/

//link de ajuda com jasmine -> https://imasters.com.br/front-end/jasmine-entendendo-os-matchers

//Jokenpo

// Atribuições

describe("CLINICARX: Teste de jokenpo123", () => {
  //-----Teste de empate
  //it("Teste PEDRA PEDRA", () => expect(jokenpo(PEDRA, PEDRA)).toEqual(EMPATE));
  //-----
  //it("Teste TESOURA TESOURA", () => expect(jokenpo(TESOURA, TESOURA)).toEqual(EMPATE));
  //-----
  //it("Teste PAPEL PAPEL", () => expect(jokenpo(PAPEL, PAPEL)).toEqual(EMPATE));
  //-----
  //-----Teste de batalha
  //it("Teste PEDRA TESOURA", () => expect(jokenpo(PEDRA, TESOURA)).toEqual(PEDRA));
  //-----
  //it("Teste TESOURA PEDRA", () => expect(jokenpo(TESOURA, PEDRA)).toEqual(PEDRA));
  //-----
  //it("Teste PEDRA PAPEL", () => expect(jokenpo(PEDRA, PAPEL)).toEqual(PAPEL));
  //-----
  //it("Teste PAPEL PEDRA", () => expect(jokenpo(PAPEL, PEDRA)).toEqual(PAPEL));
  //-----
  //it("Teste PAPEL TESOURA", () => expect(jokenpo(PAPEL, TESOURA)).toEqual(TESOURA));
  //-----
  //it("Teste TESOURA PAPEL", () => expect(jokenpo(TESOURA, PAPEL)).toEqual(TESOURA));
  //-----
  //it("Teste Jojadores desconhecidos", () => expect(jokenpo(AGULA, LAPIS)).toEqual("ERROR"));
  //-----
});
